import java.sql.*;

public class CrudJdbcWithStatement extends ConnectionJdbc {

    private String createTableQuery,patientName,patientAddress,gender;
    private Statement statement;
    private int id;


    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CrudJdbcWithStatement() {
        this.createTableQuery = null;
        this.patientName = null;
        this.patientAddress = null;
        this.gender = null;

        this.id = 0;
    }

    public String getCreateTableSQL() {
        return createTableQuery;
    }

    public void setCreateTableSQL(String createTableSQL) {
        this.createTableQuery = createTableSQL;
    }

    // Create table method
    public void createTable() {
        try {
            statement = currentconnection.createStatement();
            statement.executeUpdate(createTableQuery);

            System.out.println("Table Created Successfully");
        }
        catch (SQLException e) {
            System.out.println("Table Already exist");
        }
    }

    //Insert a row
    public void insertRow(){

        try {
            statement  = currentconnection.createStatement();
            String sql =    " INSERT INTO Patient"
                    + "(patient_id, gender, patient_name, patient_address) " + "VALUES"
                    + "('2', 'Female', 'Chelsey', '12,Pink town House')";
            statement.executeUpdate(sql);

            System.out.println("Rows inserted");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //Read Table
    public void readTable() {
        try {
            System.out.println("Reading Table Data");
            statement = currentconnection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from Patient");

            while (resultSet.next()) {

                id = resultSet.getInt(1);
                gender = resultSet.getString(2);
                patientName = resultSet.getString(3);
                patientAddress = resultSet.getString(4);


                System.out.println("Id = " + String.valueOf(id) + " | name = " + patientName + " | address = " + patientAddress+
                        " | gender = " + gender);
            }
        }
        catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    //Delete a table
    public void deleteRow(int id ) {
        try {
            statement = currentconnection.createStatement();

            String sql = "DELETE FROM Patient" + " WHERE patient_id="  + String.valueOf(id);

            int result = statement.executeUpdate(sql);

            System.out.println("Rows deleted = " + result);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Update a row
    public void updateRow(String updatedAddress,int id) {
        try {
            statement = currentconnection.createStatement();
            String sql = "Update Patient set patient_address=" + "'"+updatedAddress+"'" + " where patient_id= " + id;
            int rowsAffected    = statement.executeUpdate(sql);
            System.out.println("Rows updated = " + rowsAffected);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

