import java.sql.SQLException;
        // class over
/**
 * Created by joy on 6/5/17.
 */
public class Main {
    public static void main(String[] args) throws SQLException {

        CrudJdbcWithStatement crudJdbcWithStatement = new CrudJdbcWithStatement();

        crudJdbcWithStatement.enterConnectionDetails();
        crudJdbcWithStatement.makeConnection();

        crudJdbcWithStatement.setCreateTableSQL("CREATE TABLE Patient"+
                "(" +
                "    patient_id INT NOT NULL AUTO_INCREMENT," +
                "    gender VARCHAR(40)," +
                "    patient_name VARCHAR(100) NOT NULL," +
                "    patient_address VARCHAR(100) NOT NULL," +
                "    PRIMARY KEY ( patient_id )" +
                ")" );
        crudJdbcWithStatement.createTable();

        crudJdbcWithStatement.insertRow();
        crudJdbcWithStatement.readTable();

        crudJdbcWithStatement.updateRow("QU-120,South City",2);

        crudJdbcWithStatement.readTable();
        crudJdbcWithStatement.deleteRow(2);
    }
}

